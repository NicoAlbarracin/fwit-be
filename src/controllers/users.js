const {UsersService} = require('../services');

class UsersController {
    static async saveUser(req, res, next) {
        try {
            await UsersService.saveUser(req.body.user);
            res.send({success: true});
        } catch (err) {
            next(err);
        }
    }

    static async fetchUsers(req, res, next) {
        try {
            const users = await UsersService.fetchUsers();
            res.send({users});
        } catch (err) {
            next(err);
        }
    }

    static async modifyUser(req, res, next) {
        try {
            await UsersService.modifyUser(req.params.id, req.body.user);
            res.send({success: true});
        } catch (err) {
            next(err);
        }
    }
}

module.exports = UsersController;
