const StatusController = require('./status');
const UsersController = require('./users');


module.exports = {
    StatusController,
    UsersController
};
