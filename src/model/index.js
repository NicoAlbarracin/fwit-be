const AppInformation = require('./appInformation');
const User = require('./user');

module.exports = {
    AppInformation,
    User
};
