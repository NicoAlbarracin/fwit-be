const {User} = require('../model');

class UsersService {

    static saveUser(user) {
        const newUser = new User(user);
        newUser.earnings = parseInt(newUser.earnings);
        return newUser.save();
    }
    static fetchUsers() {
        const users = User.find({}).sort({earnings: -1}).lean().exec();
        return users;
    }
    static modifyUser(id, user){
        return User.findByIdAndUpdate(id,user).exec();
    }
}

module.exports = UsersService;
