const StatusService = require('./status');
const UsersService = require('./users');

module.exports = {
    StatusService,
    UsersService
};
