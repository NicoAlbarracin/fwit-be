const {UsersController} = require('../../controllers');

module.exports = router => {
    router.post('/', UsersController.saveUser);
    router.get('/', UsersController.fetchUsers);
    router.put('/:id', UsersController.modifyUser);
    return router;
};
