const {REDIS_HOST, REDIS_PORT, REDIS_PASSWORD} = process.env;
const cache = require('redis');
const winston = require('winston');
const {forEach, isNil, isEmpty} = require('lodash');

if (!REDIS_HOST) {
    winston.info('[Redis] Redis is not configured.');
    module.exports = null;
    return;
}

forEach(['REDIS_PREFIX', 'REDIS_TTL'], envVariable => {
    const envValue = process.env[envVariable];
    if (isNil(envValue)) {
        throw new Error(`Redis: ${envVariable} is not defined in process.env.`);
    }
    if (isEmpty(envValue)) {
        throw new Error(`Redis: ${envVariable} is empty.`);
    }
});

const client = cache.createClient({port: REDIS_PORT, host: REDIS_HOST, password: REDIS_PASSWORD});

client.on('connect', () => winston.info('[Redis] Redis connected'));
client.on('error', err => winston.error('[Redis] %s', err));

module.exports = client;
